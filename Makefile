all: deb

deb: clean
	git clone https://github.com/idan/oauthlib
	sed -i -E "s/(__version__ = '[0-9\.]*)/\1\.$(BUILD_NUMBER)/g" oauthlib/oauthlib/__init__.py
	fpm -s python -t deb -n python-oauthlib \
	-d 'python >= 2.7' \
	-d 'python-crypto' \
	-d 'python-setuptools' \
	-a all -C . \
	--deb-user 0 \
	--deb-group 0 \
	--prefix /usr \
	--python-install-lib lib/python2.7/dist-packages \
	oauthlib/setup.py

clean:
	rm -f *.deb
	rm -rf oauthlib
